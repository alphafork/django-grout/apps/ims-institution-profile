from django.core.validators import RegexValidator
from django.db import models
from ims_base.models import AbstractLog


class Institution(AbstractLog):
    PHONE_REGEX = RegexValidator(
        regex=r"^\+?1?\d{9,12}$", message="Provide valid phone number."
    )

    name = models.CharField("Name", max_length=255)

    short_description = models.TextField("Short Description", null=True, blank=True)
    about = models.TextField("About", null=True, blank=True)
    founding_date = models.DateField("Founding Year", null=True, blank=True)
    contact_1 = models.CharField(
        "Phone number", validators=[PHONE_REGEX], max_length=15, null=True, blank=True
    )
    contact_2 = models.CharField(
        "Phone number", validators=[PHONE_REGEX], max_length=15, null=True, blank=True
    )
    address = models.TextField("Address", null=True, blank=True)
    web = models.URLField("Website", max_length=200, null=True, blank=True)
    email = models.EmailField("Email", max_length=200, null=True, blank=True)
    image = models.ImageField(
        "Institution Photo", upload_to="institution_profile", null=True, blank=True
    )
    logo = models.ImageField(
        "Institution Logo", upload_to="institution_profile", null=True, blank=True
    )
