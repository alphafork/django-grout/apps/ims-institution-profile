from django.apps import AppConfig


class IMSInstitutionProfileConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "ims_institution_profile"
