# IMS Institution Profile

App to handle Institution details.


## License


  [AGPL-3.0-or-later](LICENSE)



## Contact

Alpha Fork Technologies

[connect@alphafork.com](mailto:connect@alphafork.com)
